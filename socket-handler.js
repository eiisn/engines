
class SocketHandler {

    constructor() {
        this.sockets = []
    }

    getSocket(id) {
        let index = this.sockets.findIndex(s => s.id === id);
        return this.sockets[index];
    }

    removeSocket(id) {
        let index = this.sockets.findIndex(s => s.id === id);
        this.sockets.splice(index, 1);
        return this.sockets;
    }
}

module.exports = new SocketHandler();
