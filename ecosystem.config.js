module.exports = {
    apps: [{
        name: 'Melangeur 1',
        script: './main.js',
        exp_backoff_restart_delay: 20000,
        env: {
            NODE_ENV: "production",
            PORT: 306910,
            CAPACITY: 45
        }
    },
    {
        name: 'Conditionnement 1',
        script: './main.js',
        exp_backoff_restart_delay: 20000,
        env: {
            NODE_ENV: "production",
            PORT: 306911,
            CAPACITY: 20,
            TRANSITION: JSON.stringify([
                { name: 'start', from: 'restart', to: 'start'},
                { name: 'aspiration', from: 'start', to: 'aspiration' },
                { name: 'melange', from: 'aspiration', to: 'melange' },
                { name: 'fusion', from: 'melange', to: 'fusion' },
                { name: 'transvasage', from: 'fusion', to: 'transvasage' },
                { name: 'plutonage', from: 'transvasage', to: 'plutonage' },
                { name: 'restart', from: 'plutonage', to: 'start' },
                { name: 'reset', from: ['start', 'aspiration', 'melange', 'transvasage', 'plutonage', 'restart'], to: 'restart' }
            ]),
            END_STEP: 'plutonage',
            STEP_TIME: JSON.stringify({
                min: {
                    start: 50,
                    aspiration: 200,
                    melange: 150,
                    fusion: 100,
                    transvasage: 200,
                    plutonage: 100,
                    restart: 50
                },
                max: {
                    start: 250,
                    aspiration: 500,
                    melange: 250,
                    fusion: 200,
                    transvasage: 300,
                    plutonage: 200,
                    restart: 150
                }
            }),
            METHODS: JSON.stringify({
                onAspiration: 'Aspiration en cour ...',
                onMelange: 'Melange en cour ...',
                onFusion: 'Melange en fusion ...',
                onTransvasage: 'Transvasage vers le conteneur ...',
                onRestart: 'Restart ...',
                onPlutonage: 'Plutonisation',
                onReset: '[Reset]'
            })
        }
    },
    {
        name: 'Encartonnage 1',
        script: './main.js',
        exp_backoff_restart_delay: 20000,
        env: {
            NODE_ENV: "development",
            PORT: 306912,
            CAPACITY: 45
        }
    }]
}