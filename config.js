const PORT = 3030;
const QUANTITY = 1;
const CAPACITY = 32;
const IO_NAMESPACE = '/engine';
const CONNECTION = 'connection';
const DISCONNECT = 'disconnect';
const IN_PROGRESS = 'in progress';
const QUEUED = 'queued';
const PRODUCED = 'produced';
const WAITING = 'waiting';
const SET_ORDER = 'set-order';
const ADD_QUANTITY = 'quantity';
const EMIT_STATUS = 'status';
const NEXT = 'next';
const SUCCEEDED = 'succeeded';
const FAILED = 'failed';
const ORDER_STATUS = 'order-status';
const ORDER_ERROR = 'order-error';
const GET_CAPACITY = 'get-capacity';
const GET_STATUS = 'get-status';
const RESET = 'reset-machine';
const SET_MAX_CAPACITY = 'setMaxCapacity';
const HEALTH = 'health';

const TRANSITION = [
    { name: 'start', from: 'restart', to: 'start'},
    { name: 'aspiration', from: 'start', to: 'aspiration' },
    { name: 'melange', from: 'aspiration', to: 'melange' },
    { name: 'fusion', from: 'melange', to: 'fusion' },
    { name: 'transvasage', from: 'fusion', to: 'transvasage' },
    { name: 'restart', from: 'transvasage', to: 'start' },
    { name: 'reset', from: ['start', 'aspiration', 'melange', 'transvasage', 'restart'], to: 'restart' }
]
const INIT = 'start';
const END_STEP = 'transvasage';

const STEP_TIME = {
    min: {
        start: 500,
        aspiration: 2000,
        melange: 1500,
        fusion: 1000,
        transvasage: 2000,
        restart: 500
    },
    max: {
        start: 2500,
        aspiration: 5000,
        melange: 2500,
        fusion: 2000,
        transvasage: 3000,
        restart: 1500
    }
}

const METHODS = {
    onAspiration: 'Aspiration en cour ...',
    onMelange: 'Melange en cour ...',
    onFusion: 'Melange en fusion ...',
    onTransvasage: 'Transvasage vers le conteneur ...',
    onRestart: 'Restart ...',
    onReset: '[Reset]'
}

module.exports = { 
    PORT, QUANTITY, CAPACITY, IO_NAMESPACE, CONNECTION, DISCONNECT, IN_PROGRESS, 
    QUEUED, PRODUCED, WAITING, SET_ORDER, ADD_QUANTITY, EMIT_STATUS, NEXT, SUCCEEDED, 
    FAILED, ORDER_STATUS, ORDER_ERROR, GET_CAPACITY, GET_STATUS, RESET, 
    SET_MAX_CAPACITY, HEALTH, INIT, END_STEP, TRANSITION, STEP_TIME, METHODS
}

Object.keys(module.exports).forEach(key => {
    if (process.env[key]) {
        if (key === 'METHODS') {
            module.exports.METHODS = {};
            Object.keys(JSON.parse(process.env.METHODS)).forEach(f => {
                module.exports.METHODS[f] = function () { 
                    console.log(process.env.METHODS[key]) 
                }
            });
        } else {
            console.log(key)
            console.log(process.env[key]);
            try {
                module.exports[key] = JSON.parse(process.env[key])
            } catch (err) {
                console.log(err)
                module.exports[key] = process.env[key]
            }
        }
    } else {
        if (key === 'METHODS') {
            module.exports.METHODS = {};
            Object.keys(METHODS).forEach(f => {
                module.exports.METHODS[f] = function () { 
                    console.log(METHODS[key]) 
                }
            });
        }
    }
});
