const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const stateMachine = require('javascript-state-machine');
const Queue = require('bee-queue');

const SocketHandler = require('./socket-handler');
const config = require('./config');

const port = process.argv[2] || config.PORT;
const orderQueue = new Queue(`order-${port}`);

const fsm = new stateMachine({
    init: config.INIT,
    transitions: config.TRANSITION,
    methods: config.METHODS
});

const orderJob = (data, done) => {
    const socket = SocketHandler.getSocket(data.socket);
    console.log(`[Order:${data.order}] Step: ${fsm.state}`);
    const timeout = Math.random() * config.STEP_TIME.max[fsm.state] + config.STEP_TIME.min[fsm.state];
    console.log(`[Order:${data.order}] Next step in: ${timeout}`);
    if (socket) {
        setTimeout(() => {
            if (fsm.state === config.END_STEP) {
                const res = {
                    order: data.order,
                    code: data.code,
                    quantity: config.QUANTITY
                }
                socket.emit(config.ADD_QUANTITY, res);
                data.quantity = data.quantity - config.QUANTITY;
                if (data.quantity <= 0) {
                    fsm.reset();
                    socket.emit(config.NEXT, fsm.state);
                    return done(null, res);
                }
            }
            console.log(`[Order:${data.order}] Step transitions: ${fsm.transitions()}`);
            fsm[fsm.transitions()[0]]();
            socket.emit(config.NEXT, fsm.state);
            return orderJob(data, done);
        }, timeout);
    } else {
        return done('Socket disconnected');
    }
}

const emitHealth = (socket) => {
    orderQueue.ready().then(async (queue) => {
        const checkHealth = await queue.checkHealth();
        socket.emit('health', checkHealth);
    });
}

orderQueue.process((job, done) => {
    console.log("Process order: ", job.data.order);
    const socket = SocketHandler.getSocket(job.data.socket);
    if (socket) {
        console.log('Socket: [', socket.connected ? 'ok' : 'ko', ']');
        socket.emit(config.ORDER_STATUS, {
            order: job.data.order,
            status: config.IN_PROGRESS
        });
        emitHealth(socket);
        orderJob(job.data, done);
    } else {
        console.log('Socket: ', socket);
        return done('Undefined socket: ', socket);
    }
});

const nspEngine = io.of(config.IO_NAMESPACE);

nspEngine.on(config.CONNECTION, (socket) => {
    console.log("Server connected");
    SocketHandler.sockets.push(socket);

    let emitHealthInterval = setInterval(() => {
        emitHealth(socket);
    }, 15000);

    socket.on(config.DISCONNECT, () => {
        SocketHandler.removeSocket(socket.id);
        clearInterval(emitHealthInterval);
        console.log("Server disconnected");
    });

    socket.on(config.GET_STATUS, () => {
        socket.emit(config.NEXT, fsm.state);
        socket.emit(config.EMIT_STATUS, true);
        emitHealth(socket);
    });

    socket.on('check-health', () => {
        emitHealth(socket);
    });

    socket.on(config.RESET, () => {
        fsm.reset();
    });

    socket.on(config.GET_CAPACITY, () => {
        socket.emit(config.SET_MAX_CAPACITY, config.CAPACITY);
    });

    socket.on(config.SET_ORDER, (order) => {
        /** 
         * order: { product: { name: str, code: int } }, quantity: int, _id: ObjectId }
        */
        console.log('Order received: ', order);
        if (order) {
            console.log('Create job');

            const job = orderQueue.createJob({
                socket: socket.id,
                order: order._id,
                code: order.product.code,
                quantity: order.quantity - order.produced
            });

            job.on(config.SUCCEEDED, (result) => {
                console.log("Success: ", result);
                socket.emit(config.ORDER_STATUS, {
                    order: result.order,
                    status: config.PRODUCED
                });
            });

            job.on(config.FAILED, (err) => {
                if (socket) {
                    socket.emit(config.ORDER_ERROR, {
                        order: order,
                        error: err
                    });
                } else {
                    console.log(err);
                }
            });

            job.save();

            socket.emit(config.ORDER_STATUS, {
                order: order._id,
                status: config.QUEUED
            });
        }
    });

    socket.emit(config.SET_MAX_CAPACITY, config.CAPACITY);
    socket.emit(config.NEXT, fsm.state);
    console.log('Sockets: ', SocketHandler.sockets.map(s => s.id))
});

http.listen(port, () => {
    console.log("Engine listening on port: ", port);
});
